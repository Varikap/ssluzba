insert into authority (role) values ('ADMIN');
insert into authority (role) values ('STUDENT');
insert into authority (role) values ('PREDAVAC');
	
-- password 'admin'
insert into user (username, password) values ('admin', '$2a$04$SwzgBrIJZhfnzOw7KFcdzOTiY6EFVwIpG7fkF/D1w26G1.fWsi.aK');

-- password 'predavac'
insert into user (username, password) values ('predavac', '$2a$10$GjKr.racHKsFW39LdxX0JOKWzcvdRYZedPGRBOMGTKMBXV0JlkLy6');

-- password 'user'
insert into user (username, password) values ('user', '$2a$04$Amda.Gm4Q.ZbXz9wcohDHOhOBaNQAkSS1QO26Eh8Hovu3uzEpQvcq');


insert into user_authority (user_id, authority_id) values (1, 1);
insert into user_authority (user_id, authority_id) values (2, 3);
insert into user_authority (user_id, authority_id) values (3, 2);

