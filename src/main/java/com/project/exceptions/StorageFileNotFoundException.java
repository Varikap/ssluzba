package com.project.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class StorageFileNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = -5216089899746026762L;

	public StorageFileNotFoundException(String message) {
	        super(message);
	    }

	    public StorageFileNotFoundException(String message, Throwable cause) {
	        super(message, cause);
	    }
}
