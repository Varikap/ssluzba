package com.project.dto;

import java.util.Date;

import com.project.model.Rok;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RokDTO {
	private Long id;
	private String naziv;
	private Date datumPocetka;
	private Date datumZavrsetka;
	private Long cena;
	
	public RokDTO(Rok r) {
		this.id = r.getId();
		this.naziv = r.getNaziv();
		this.datumPocetka = r.getDatumPocetka();
		this.datumZavrsetka = r.getDatumZavrsetka();
		this.cena = r.getCena();
	}
}
