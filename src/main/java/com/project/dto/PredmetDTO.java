package com.project.dto;

import com.project.model.Predmet;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PredmetDTO {
	private Long id;
	private String name;
	private String oznaka;
	private Integer ESPB;
	private Integer godina;
	private Long smerId;
	private SmerDTO smer;

	public PredmetDTO (Predmet p) {
		this.id = p.getId();
		this.name = p.getName();
		this.oznaka = p.getOznaka();
		this.ESPB = p.getESPB();
		this.godina = p.getGodina();
		this.smerId = p.getSmer().getId();
		this.smer = new SmerDTO(p.getSmer());
	}
	
}
