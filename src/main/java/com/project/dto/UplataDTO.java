package com.project.dto;

import com.project.model.Uplata;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UplataDTO {
	private Double iznos;
	private String svrhaUplate;
	
	public UplataDTO(Uplata u) {
		this.iznos = u.getIznos();
		this.svrhaUplate = u.getSvrhaUplate();
	}
}
