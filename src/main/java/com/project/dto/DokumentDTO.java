package com.project.dto;

import java.util.List;

import com.project.model.Dokument;
import com.project.model.Predavac;
import com.project.model.Predmet;
import com.project.model.Student;
import com.project.model.enums.Zvanje;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DokumentDTO {
	private Long id;
	private String name;
	private StudentIDTO student;
	
	public DokumentDTO(Dokument d) {
		this.id=d.getId();
		this.name=d.getName();
		this.student = new StudentIDTO(d.getStudent());
	}
	
	@Data
	@NoArgsConstructor
	public static class StudentIDTO {
		private Long id;
		private String name;
		private String surname;
		private String indexBr;

		public StudentIDTO(Student s) {
			this.id = s.getId();
			this.name = s.getName();
			this.surname = s.getSurname();
			this.indexBr = s.getIndexBr();
		}
	}
}
