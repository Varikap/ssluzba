package com.project.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.project.model.enums.Role;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginDTO {
	@NotNull
	private String username;
	@NotNull
	@Size(max=32, message = "Maximum32 char")
	private String password;
	private Role role;
}
