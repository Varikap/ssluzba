package com.project.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateAdminDTO {
	private String name;
	
	private String surname;
	
	private String jmbg;
	
	private String email;
	
	private String username;
	
	private String password;
}
