package com.project.dto;

import java.util.ArrayList;
import java.util.List;

import com.project.model.Predmet;
import com.project.model.Smer;
import com.project.model.Student;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentDTO {
	private Long id;
	private String name;
	private String surname;
	private String jmbg;
	private String indexBr;
	private SmerIDTO smer;
	private String role;
	private Integer godina;
	private Double stanjeNaRacunu;
	private List<PredmetIDTO> predmeti = new ArrayList<PredmetIDTO>();
	
	public StudentDTO(Student student) {
		this.id = student.getId();
		this.name = student.getName();
		this.surname = student.getSurname();
		this.jmbg = student.getJmbg();
		this.godina = student.getGodina();
		this.stanjeNaRacunu = student.getStanjeNaRacunu();
		this.indexBr = student.getIndexBr();
		this.smer = new SmerIDTO(student.getSmer());
		for (Predmet p : student.getPohadja()) {
			this.predmeti.add(new PredmetIDTO(p));
		}
	}
	
	@Data
	@NoArgsConstructor
	public static class SmerIDTO {
		private Long id;
		private String naziv;
		
		public SmerIDTO(Smer smer) {
			this.id = smer.getId();
			this.naziv = smer.getNaziv();
		}
	}
	
	@Data
	@NoArgsConstructor
	public static class PredmetIDTO {
		private Long id;
		private String name;
		
		public PredmetIDTO(Predmet predmet) {
			this.id = predmet.getId();
			this.name = predmet.getName();
		}
		
	}
}
