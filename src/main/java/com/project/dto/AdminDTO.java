package com.project.dto;

import com.project.model.Admin;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdminDTO {
	
	private Long id;
	
	private String name;
	
	private String surname;
	
	private String jmbg;
	
	private String email;
	
	private String role;
	
	public AdminDTO(Admin a) {
		this.id = a.getId();
		this.name = a.getName();
		this.surname = a.getSurname();
		this.email = a.getEmail();
		this.jmbg = a.getEmail();
	}
}
