package com.project.dto;

import java.util.ArrayList;
import java.util.List;

import com.project.model.Predmet;
import com.project.model.Smer;
import com.project.model.Student;
import com.project.model.enums.NivoStudija;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SmerDTO {
	private Long id;
	private String naziv;
	private Integer trajanje;
	private Integer ESPB;
	private NivoStudija nivo;
	private List<PredmetIDTO> predmeti = new ArrayList<PredmetIDTO>();
	private List<StudentIDTO> studenti = new ArrayList<StudentIDTO>();
	
	
	public SmerDTO (Smer s) {
		this.id = s.getId();
		this.naziv = s.getNaziv();
		this.trajanje = s.getTrajanje();
		this.ESPB = s.getESPB();
		for (Predmet pr : s.getPredmeti()) {
			this.predmeti.add(new PredmetIDTO(pr));
		}
		for (Student st : s.getStudenti()) {
			this.studenti.add(new StudentIDTO(st));
		}
		this.nivo = s.getNivo();
		switch (s.getNivo()) {
			case STRUKOVNE: {
				this.trajanje = 3;
				break;
			}
			case DOKTORSKE: {
				this.trajanje = 2;
				break;
			}
			case MASTER: {
				this.trajanje = 1;
				break;
			}
			case OSNOVNE: {
				this.trajanje = 4;
				break;
			}
			case ÜBERMEISTERSCHNITZELNLACHEN: {
				this.trajanje = 2;
				break;
			}
			default: {
				throw new IllegalArgumentException("Smer nivo not valid");
			}
		}
	}
	
	@Data
	@NoArgsConstructor
	public static class PredmetIDTO {
		private Long id;
		private String name;

		public PredmetIDTO(Predmet predmet) {
			this.id = predmet.getId();
			this.name = predmet.getName();
		}
	}
	
	@Data
	@NoArgsConstructor
	public static class StudentIDTO {
		private Long id;
		private String name;

		public StudentIDTO(Student predmet) {
			this.id = predmet.getId();
			this.name = predmet.getName();
		}
	}
}
