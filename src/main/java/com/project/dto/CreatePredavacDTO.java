package com.project.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.project.model.enums.Zvanje;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreatePredavacDTO {
	@NotNull
	private String name;
	@NotNull
	private String surname;
	@NotNull
	@Email
	private String email;
	@NotNull
	private String jmbg;
	@NotNull
	private Zvanje zvanje;
	@NotNull
	private String username;
	@NotNull
	private String password;
}
