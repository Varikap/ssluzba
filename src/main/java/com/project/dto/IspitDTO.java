package com.project.dto;

import java.util.Date;
import java.util.List;

import com.project.dto.DokumentDTO.StudentIDTO;
import com.project.model.Ispit;
import com.project.model.Predavac;
import com.project.model.Predmet;
import com.project.model.Student;
import com.project.model.enums.Zvanje;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IspitDTO {
	private Long id;
	private Date datum_polaganja;
	private Integer ocena;
	private PredmetIDTO predmet;
	private Long predmetID;
	private Long rokID;
	private RokDTO rokDTO;
	private PredavaccDTO predavacDTO;
	
	public IspitDTO (Ispit i) {
		this.id = i.getId();
		this.datum_polaganja = i.getDatum_polaganja();
		this.ocena = i.getOcena();
		this.predmet = new PredmetIDTO(i.getPredmet());
		this.rokDTO = new RokDTO(i.getRok());
		this.predavacDTO = new PredavaccDTO(i.getPredmet().getPredavaci());
		
	}
	
	@Data
	@NoArgsConstructor
	public static class PredmetIDTO {
		private Long id;
		private String name;
		private String description;
		private Integer ESPB;

		public PredmetIDTO(Predmet p) {
			this.id = p.getId();
			this.name = p.getName();
			this.description = p.getOznaka();
			this.ESPB = p.getESPB();
		}
	}
	
	@Data
	@NoArgsConstructor
	private static class PredavaccDTO {
		private Long id;
		private String name;
		private String surname;
		private String email;
		private String zvanje;
		
		public PredavaccDTO(List<Predavac> predavaci) {
			for (Predavac p : predavaci) {
				if(p.getZvanje() == Zvanje.NASTAVNIK) {
					this.id = p.getId();
					this.name = p.getName();
					this.surname = p.getSurname();
					this.email = p.getEmail();
					this.zvanje = p.getZvanje().toString();
				}
			}
		}
	}
}
