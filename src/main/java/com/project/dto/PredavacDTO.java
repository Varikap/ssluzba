package com.project.dto;

import java.util.ArrayList;
import java.util.List;

import com.project.model.Predavac;
import com.project.model.Predmet;
import com.project.utils.Const;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PredavacDTO {
	private Long id;
	private String name;
	private String surname;
	private String email;
	private String jmbg;
	private String zvanje;
	private String role;
	private List<PredmetIDTO> predaje = new ArrayList<PredmetIDTO>();

	public PredavacDTO(Predavac p) {
		this.id = p.getId();
		this.name = p.getName();
		this.surname = p.getSurname();
		this.jmbg = p.getJmbg();
		this.email = p.getEmail();
		for (Predmet pr : p.getPredaje()) {
			this.predaje.add(new PredmetIDTO(pr));
		}

		switch (p.getZvanje()) {
			case ASISTENT: {
				this.zvanje = Const.ASISTENT;
				break;
			}
			case NASTAVNIK: {
				this.zvanje = Const.NASTAVNIK;
				break;
			}
			case DEMONSTRATOR: {
				this.zvanje = Const.DEMONSTRATOR;
				break;
			}
			default: {
				throw new IllegalArgumentException("Predavac tip not valid");
			}
		}
	}

	@Data
	@NoArgsConstructor
	public static class PredmetIDTO {
		private Long id;
		private String name;

		public PredmetIDTO(Predmet predmet) {
			this.id = predmet.getId();
			this.name = predmet.getName();
		}
	}
}
