package com.project.dto;

import java.util.List;

import lombok.Data;

@Data
public class PrijaviDTO {
	List<Long> ispitIDs;
}
