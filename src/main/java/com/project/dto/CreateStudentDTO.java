package com.project.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateStudentDTO {
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private String surname;
	@NotNull
	private String jmbg;
	@NotNull
	private String indexBr;
	private Double stanjeNaRacunu;
	@NotNull
	private Long smerId;
	@NotNull
	private Integer godina;
}
