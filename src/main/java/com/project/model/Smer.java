package com.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.project.dto.SmerDTO;
import com.project.model.enums.NivoStudija;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "smer")
@Data
@NoArgsConstructor
public class Smer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private NivoStudija nivo;
	@Column 
	private Integer trajanje; //semsestri
	@Column
	private Integer ESPB;
	@OneToMany(mappedBy = "smer")
	private List<Student> studenti = new ArrayList<Student>();
	@OneToMany(mappedBy = "smer")
	private List<Predmet> predmeti = new ArrayList<Predmet>();
	

	
	public Smer(String naziv, NivoStudija nivo, Integer trajanje, Integer ESPB) {
		super();
		this.naziv = naziv;
		this.nivo = nivo;
		this.trajanje = trajanje;
		this.ESPB = ESPB;//hahaha autogen konstruktor se utripOo
	}
	
	public void addPredmet(Predmet p) {
		predmeti.add(p);
		p.setSmer(this);
	}

	public void delPredmet(Predmet p) {
		predmeti.remove(p);
	}
}
