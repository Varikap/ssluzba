package com.project.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "dokument")
@Data
@NoArgsConstructor
public class Dokument {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String name;
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Student student;
	
	
	public Dokument(String name, Student student) {
		super();
		this.name = name;
		this.student = student;
	}
}
