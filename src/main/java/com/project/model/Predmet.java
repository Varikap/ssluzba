package com.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.project.dto.PredmetDTO;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "predmet")
@Data
@NoArgsConstructor
public class Predmet {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String name;
	@Column
	private String oznaka;
	@Column
	private Integer ESPB;
	@Column
	private Integer godina;
	@ManyToMany(mappedBy = "pohadja")
	private List<Student> studenti = new ArrayList<Student>();
	@ManyToMany(mappedBy = "predaje")
	private List<Predavac> predavaci = new ArrayList<Predavac>();
	@ManyToOne
	private Smer smer;
	@OneToMany(mappedBy = "predmet")
	private List<Ispit> ispiti = new ArrayList<Ispit>();
	
	//TODO add metode


	public Predmet(String name, Smer smer,String description, Integer ESPB, Integer godina) {
		super();
		this.name = name;
		this.smer = smer;
		this.oznaka = description;
		this.ESPB = ESPB;
		this.godina = godina;
	}
}
