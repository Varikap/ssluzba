package com.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.project.dto.PredavacDTO;
import com.project.model.enums.Zvanje;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "predavac")
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class Predavac{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String name;
	@Column
	private String surname;
	@Column
	private String email;
	@Column
	private String jmbg;
	@Column
	private Zvanje zvanje;
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "predavac_predmet", joinColumns = @JoinColumn(name = "predavac_id"), inverseJoinColumns = @JoinColumn(name = "predmet_id"))
	private List<Predmet> predaje = new ArrayList<Predmet>();
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id") 
	private User user;
	
	
	public Predavac(String name, String surname, String jmbg, Zvanje zvanje, User user,String email) {
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.zvanje = zvanje;
		this.user = user;
		this.email = email;
	}
	
	public Predavac(String name, String surname, String jmbg, Zvanje zvanje, String email) {
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.zvanje = zvanje;
		this.email = email;
	}

	public void addPredmet(Predmet p) {
		predaje.add(p);
		p.getPredavaci().add(this);
	}

	public void delPredmet(Predmet p) {
		predaje.remove(p);
		p.getPredavaci().remove(this);
	}
}
