package com.project.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="rok")
@Getter @Setter
@NoArgsConstructor
public class Rok {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String naziv;
	@Column
	private Date datumPocetka;
	@Column
	private Date datumZavrsetka;
	@Column
	private Long cena;
	@OneToMany(mappedBy = "rok")
	private List<Ispit> ispiti;
	public Rok(String naziv, Date datumPocetka, Date datumZavrsetka, int cena) {
		super();
		this.naziv = naziv;
		this.datumPocetka = datumPocetka;
		this.datumZavrsetka = datumZavrsetka;
		this.cena = Long.valueOf(cena);
	}
	
	
}
