package com.project.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "admin")
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class Admin {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String name;
	@Column
	private String surname;
	@Column
	private String jmbg;
	@Column
	private String email;
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id") 
	private User user;
	
	public Admin(String name, String surname, String jmbg, String email, User user) {
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.email = email;
		this.user = user;
	}
	
	public Admin(String name, String surname, String jmbg, String email) {
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.email = email;
	}
}
