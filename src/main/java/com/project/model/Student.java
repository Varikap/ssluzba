package com.project.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.project.dto.StudentDTO;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "student")
@Getter @Setter
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String name;
	@Column
	private String surname;
	@Column
	private String jmbg;
	@Column
	private String indexBr;
	@Column
	private Integer godina;
	@Column
	private Double stanjeNaRacunu;
	@ManyToOne
	private Smer smer;	
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "student_predmet", joinColumns = @JoinColumn(name = "student_id"), inverseJoinColumns = @JoinColumn(name = "predmet_id"))
	private List<Predmet> pohadja = new ArrayList<Predmet>();
	@OneToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id") 
	private User user;
	@ManyToMany(mappedBy = "studenti")
	private List<Ispit> ispiti = new ArrayList<>();
	
	
	public void addPredmet(Predmet p) {
		pohadja.add(p);
	}

	public void delPredmet(Predmet p) {
		pohadja.remove(p);
	}
	
	public void addIspit(Ispit i) {
		ispiti.add(i);
	}
	
	public void delIspit(Ispit i) {
		ispiti.remove(i);
	}
	public Student(String name, String surname, String jmbg, String indexBr, Smer smer, User user) {
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.indexBr = indexBr;
		this.smer = smer;
		this.user = user;
	}
	
	public Student(String name, String surname, String jmbg, String indexBr, Smer smer) {
		this.name = name;
		this.surname = surname;
		this.jmbg = jmbg;
		this.indexBr = indexBr;
		this.smer = smer;
	}
}
