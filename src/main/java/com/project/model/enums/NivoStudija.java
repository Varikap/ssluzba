package com.project.model.enums;

public enum NivoStudija {
		STRUKOVNE,
		OSNOVNE,
		MASTER,
		ÜBERMEISTERSCHNITZELNLACHEN,
		DOKTORSKE
}
