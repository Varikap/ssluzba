package com.project.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.project.dto.IspitDTO;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ispit")
@Data
@NoArgsConstructor
public class Ispit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private Date datum_polaganja;
	@Column
	private Integer ocena;
	@ManyToMany(cascade = {CascadeType.MERGE })
	@JoinTable(name="student_ispit", joinColumns = @JoinColumn(name="ispit_id"), inverseJoinColumns = @JoinColumn(name="student_id"))
	private List<Student> studenti = new ArrayList<>();
	@ManyToOne
	private Predmet predmet;
	@ManyToOne
	private Rok rok;
	
	public Ispit(Date datum_polaganja, Integer ocena, Student student, Predmet predmet) {
		super();
		this.datum_polaganja = datum_polaganja;
		this.ocena = ocena;
		this.studenti.add(student);
		this.predmet = predmet;
	}
	
	public void addStudent(Student s) {
		this.studenti.add(s);
	}
}
