package com.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "uplata")
@Data
@NoArgsConstructor
public class Uplata {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	@Column
	private String svrhaUplate;
	@Column
	private double iznos;
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Student student;

	public Uplata(String svrhaUplate, double iznos) {
		super();
		this.svrhaUplate = svrhaUplate;
		this.iznos = iznos;
	}
	
	public Uplata(String svrhaUplate, double iznos, Student st) {
		super();
		this.svrhaUplate = svrhaUplate;
		this.iznos = iznos;
		this.student = st;
	}
}
