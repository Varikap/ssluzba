package com.project.converters;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.CreateStudentDTO;
import com.project.model.Predmet;
import com.project.model.Smer;
import com.project.model.Student;
import com.project.service.SmerService;

@Component
public class CreateStudentConverter implements Converter<CreateStudentDTO, Student> {
	@Autowired
	private SmerService smerService;

	@Override
	public Student convert(CreateStudentDTO source) {
		Student s = new Student();
		s.setId(source.getId());
		s.setIndexBr(source.getIndexBr());
		s.setJmbg(source.getJmbg());
		s.setName(source.getName());
		s.setSurname(source.getSurname());
		s.setGodina(source.getGodina());
		if(source.getStanjeNaRacunu() != null)
			s.setStanjeNaRacunu(source.getStanjeNaRacunu());
		else
			s.setStanjeNaRacunu(0.0);
		Smer smer = smerService.findOne(source.getSmerId());
		s.setSmer(smer);
		List<Predmet> predmeti = smer.getPredmeti().stream()
				.filter(p -> p.getGodina() == s.getGodina())
				.collect(Collectors.toList());
		s.setPohadja(predmeti);
		
		return s;
	}
}
