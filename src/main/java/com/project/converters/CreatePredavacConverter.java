package com.project.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.CreatePredavacDTO;
import com.project.model.Predavac;

@Component
public class CreatePredavacConverter implements Converter<CreatePredavacDTO, Predavac> {

	@Override
	public Predavac convert(CreatePredavacDTO source) {
		Predavac p = new Predavac();
		p.setName(source.getName());
		p.setSurname(source.getSurname());
		p.setEmail(source.getEmail());
		p.setJmbg(source.getJmbg());
		p.setZvanje(source.getZvanje());
		return p;
	}
}
