package com.project.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.CreateAdminDTO;
import com.project.model.Admin;

@Component
public class CreateAdminConverter implements Converter<CreateAdminDTO, Admin> {

	@Override
	public Admin convert(CreateAdminDTO source) {
		Admin a = new Admin();
		a.setEmail(source.getEmail());
		a.setJmbg(source.getJmbg());
		a.setName(source.getName());
		a.setSurname(source.getSurname());
		return a;
	}

}
