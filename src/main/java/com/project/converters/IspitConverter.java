package com.project.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.IspitDTO;
import com.project.model.Ispit;
import com.project.service.PredmetService;
import com.project.service.RokService;

@Component
public class IspitConverter implements Converter<IspitDTO, Ispit>{

	@Autowired
	private PredmetService predmetS;
	@Autowired
	private RokService rokS;
	@Override
	public Ispit convert(IspitDTO s) {
		Ispit i = new Ispit();
		i.setId(s.getId());
		i.setDatum_polaganja(s.getDatum_polaganja());
		i.setOcena(s.getOcena());
		i.setPredmet(predmetS.findOne(s.getPredmetID()));
		i.setRok(rokS.findOne(s.getRokID()));
		return i;
	}
}
