package com.project.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.PredavacDTO;
import com.project.dto.PredavacDTO.PredmetIDTO;
import com.project.model.Predavac;
import com.project.model.Predmet;
import com.project.model.enums.Zvanje;
import com.project.service.PredmetService;
import com.project.utils.Const;

@Component
public class PredavacConverter implements Converter<PredavacDTO, Predavac>  {
	@Autowired
	private PredmetService predmetS;
	@Override
	public Predavac convert(PredavacDTO s) {
		Predavac p = new Predavac();
		p.setId(s.getId());
		p.setJmbg(s.getJmbg());
		p.setName(s.getName());
		p.setSurname(s.getSurname());
		p.setEmail(s.getEmail());
		switch (s.getZvanje()) {
		case Const.ASISTENT:{
			p.setZvanje(Zvanje.ASISTENT);
			break;
		}
		case Const.NASTAVNIK:{
			p.setZvanje(Zvanje.NASTAVNIK);
			break;
		}
		case Const.DEMONSTRATOR:{
			p.setZvanje(Zvanje.DEMONSTRATOR);
			break;
		}
		default:
			throw new IllegalArgumentException("Predavac tip not valid");
		}
		List<Predmet> predmeti = new ArrayList<Predmet>();
		for(PredmetIDTO pr : s.getPredaje()) {			
			predmeti.add(predmetS.findOne(pr.getId()));
	}
		p.setPredaje(predmeti);
		return p;
	}
}
