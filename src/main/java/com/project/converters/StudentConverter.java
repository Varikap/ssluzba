package com.project.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.StudentDTO;
import com.project.dto.StudentDTO.PredmetIDTO;
import com.project.model.Predmet;
import com.project.model.Student;
import com.project.service.PredmetService;
import com.project.service.SmerService;

@Component
public class StudentConverter implements Converter<StudentDTO, Student>  {
	@Autowired
	private SmerService smerS;
	@Autowired
	private PredmetService predmetS;
	@Override
	public Student convert(StudentDTO source) {
		Student s = new Student();
		s.setId(source.getId());
		s.setGodina(source.getGodina());
		s.setStanjeNaRacunu(source.getStanjeNaRacunu());
		s.setIndexBr(source.getIndexBr());
		s.setJmbg(source.getJmbg());
		s.setName(source.getName());
		s.setSurname(source.getSurname());
		s.setSmer(smerS.findOne(source.getSmer().getId()));
		
		List<Predmet> predmeti = new ArrayList<Predmet>();
		for(PredmetIDTO p : source.getPredmeti()) {			
				predmeti.add(predmetS.findOne(p.getId()));
		}
		s.setPohadja(predmeti);
		return s;
	}
}
