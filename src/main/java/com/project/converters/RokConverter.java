package com.project.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.RokDTO;
import com.project.model.Rok;

@Component
public class RokConverter implements Converter<RokDTO, Rok>{

	@Override
	public Rok convert(RokDTO s) {
		Rok r = new Rok();
		r.setId(s.getId());
		r.setCena(s.getCena());
		r.setDatumPocetka(s.getDatumPocetka());
		r.setDatumZavrsetka(s.getDatumZavrsetka());
		r.setNaziv(s.getNaziv());
		return r;
	}

}
