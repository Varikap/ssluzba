package com.project.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.SmerDTO;
import com.project.dto.SmerDTO.PredmetIDTO;
import com.project.dto.SmerDTO.StudentIDTO;
import com.project.model.Predmet;
import com.project.model.Smer;
import com.project.model.Student;
import com.project.model.enums.NivoStudija;
import com.project.service.PredmetService;
import com.project.service.StudentService;
import com.project.utils.Const;

@Component
public class SmerConverter implements Converter<SmerDTO, Smer>{
	
	@Autowired
	private PredmetService predmetS;
	@Autowired
	private StudentService studentS;
	@Override
	public Smer convert(SmerDTO st) {
		Smer s = new Smer();
		s.setId(st.getId());
		s.setNaziv(st.getNaziv());
		s.setTrajanje(st.getTrajanje());
		s.setESPB(st.getESPB());
		s.setNivo(st.getNivo());
		List<Student> studenti = new ArrayList<Student>();
		for(StudentIDTO sr : st.getStudenti()) {			
			studenti.add(studentS.findOne(sr.getId()));
	}
		List<Predmet> predmeti = new ArrayList<Predmet>();
		for(PredmetIDTO pr : st.getPredmeti()) {			
			predmeti.add(predmetS.findOne(pr.getId()));
	}
		s.setStudenti(studenti);
		s.setPredmeti(predmeti);
		return s;
	}
}
