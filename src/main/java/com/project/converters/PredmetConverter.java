package com.project.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.PredmetDTO;
import com.project.model.Predmet;

@Component
public class PredmetConverter implements Converter<PredmetDTO, Predmet> {
	
	@Override
	public Predmet convert(PredmetDTO source) {
		Predmet p = new Predmet();
		p.setId(source.getId());
		p.setOznaka(source.getOznaka());
		p.setESPB(source.getESPB());
		p.setName(source.getName());
		p.setGodina(source.getGodina());
		
		return p;
	}

}
