package com.project.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.AdminDTO;
import com.project.model.Admin;

@Component
public class AdminConverter implements Converter<AdminDTO, Admin> {

	@Override
	public Admin convert(AdminDTO source) {
		Admin a = new Admin();
		a.setId(source.getId());
		a.setEmail(source.getEmail());
		a.setJmbg(source.getJmbg());
		a.setName(source.getName());
		a.setSurname(source.getSurname());
		return a;
	}

}
