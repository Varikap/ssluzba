package com.project.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.project.dto.DokumentDTO;
import com.project.model.Dokument;
import com.project.service.StudentService;

@Component
public class DokumentConverter implements Converter<DokumentDTO, Dokument>  {
	@Autowired
	private StudentService studentS;
	@Override
	public Dokument convert(DokumentDTO s) {
		Dokument d = new Dokument();
		d.setId(s.getId());
		d.setName(s.getName());
		d.setStudent(studentS.findOne(s.getStudent().getId()));
		return d;
	}
}
