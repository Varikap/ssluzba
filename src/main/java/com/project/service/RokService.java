package com.project.service;

import java.util.List;

import com.project.model.Rok;

public interface RokService {
	Rok findOne(Long id);
	
	List<Rok> findAll();
	
	Rok save(Rok rok);
	
	void remove(Long id);
}
