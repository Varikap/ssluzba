package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Admin;
import com.project.model.User;

public interface AdminService {
	Admin findOne(Long id);

	List<Admin> findAll();
	
	Page<Admin> findAll(Pageable page);

	Admin save(Admin admin);

	void remove(Long id);
	
	Admin findByUser(User user);
}
