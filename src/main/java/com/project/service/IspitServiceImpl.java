package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Ispit;
import com.project.repository.IspitRepository;

@Service
public class IspitServiceImpl implements IspitService {

	@Autowired
	IspitRepository repo;

	public Ispit findOne(Long id) {
		return repo.findById(id).get();
	}

	public List<Ispit> findAll() {
		return repo.findAll();
	}

	public Ispit save(Ispit ispit) {
		return repo.save(ispit);
	}

	public void remove(Long id) {
		repo.deleteById(id);
	}

	public Page<Ispit> findAll(Pageable page) {
		return repo.findAll(page);
	}

}
