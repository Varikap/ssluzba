package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.model.Rok;
import com.project.repository.RokRepository;

@Service
public class RokServiceImpl implements RokService {
	@Autowired
	private RokRepository repo;

	@Override
	public Rok findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Rok> findAll() {
		return repo.findAll();
	}

	@Override
	public Rok save(Rok rok) {
		return repo.save(rok);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

}
