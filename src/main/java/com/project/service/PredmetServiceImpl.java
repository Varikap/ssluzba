package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Predmet;
import com.project.repository.PredmetRepository;

@Service
public class PredmetServiceImpl implements PredmetService {
	
	@Autowired
	PredmetRepository repo;

	public Predmet findOne(Long id) {
		return repo.findById(id).get();
	}

	public List<Predmet> findAll() {
		return repo.findAll();
	}

	public Predmet save(Predmet predmet) {		
		return repo.save(predmet);
	}

	public void remove(Long id) {
		repo.deleteById(id);
	}

	public Page<Predmet> findAll(Pageable page) {
		return findAll(page);
	}

}
