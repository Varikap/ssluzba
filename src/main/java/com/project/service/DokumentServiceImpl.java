package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Dokument;
import com.project.repository.DokumentRepository;

@Service
public class DokumentServiceImpl implements DokumentService {
	
	@Autowired
	DokumentRepository repo;

	@Override
	public Dokument findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Dokument> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Dokument> findAll(Pageable page) {
		return findAll(page);
	}

	@Override
	public Dokument save(Dokument dokument) {
		return repo.save(dokument);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}
}
