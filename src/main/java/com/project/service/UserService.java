package com.project.service;

import com.project.model.User;
import com.project.model.enums.Role;

public interface UserService {
	User save(String username, String password, Role role);
	
	User findByUsername(String username);
	
	User save(User u);
}
