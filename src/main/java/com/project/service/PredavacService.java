package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.dto.IspitDTO;
import com.project.dto.PredmetDTO;
import com.project.model.Predavac;
import com.project.model.User;

public interface PredavacService {
	Predavac findOne(Long id);

	List<Predavac> findAll();
	
	Page<Predavac> findAll(Pageable page);

	Predavac save(Predavac predavac);

	void remove(Long id);
	
	Predavac findByUser(User user);
	
	List<PredmetDTO> getPredmeti(String token);
	
	List<IspitDTO> getZakazane(String token);
	
	IspitDTO oceni(String token, Long id, Integer ocena);
}
