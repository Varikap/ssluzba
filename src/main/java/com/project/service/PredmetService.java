package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Predmet;

public interface PredmetService {
	Predmet findOne(Long id);

	List<Predmet> findAll();
	
	Page<Predmet> findAll(Pageable page);

	Predmet save(Predmet predmet);

	void remove(Long id);
}