package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Admin;
import com.project.model.User;
import com.project.repository.AdminRepository;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminRepository repo;

	@Override
	public Admin findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Admin> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Admin> findAll(Pageable page) {		
		return repo.findAll(page);
	}

	@Override
	public Admin save(Admin admin) {
		return repo.save(admin);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

	@Override
	public Admin findByUser(User user) {
		return repo.findByUser(user);
	}

}
