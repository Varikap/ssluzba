package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.dto.PrijaviDTO;
import com.project.dto.UplataDTO;
import com.project.model.Ispit;
import com.project.model.Student;
import com.project.model.Uplata;
import com.project.model.User;

public interface StudentService {
	Student findOne(Long id);

	List<Student> findAll();
	
	Page<Student> findAll(Pageable page);
	
	Student save(Student student);
	
	void remove(Long id);
	
	List<Student> findAllByIme(String ime);
	
	List<Student> findAllByPrezime(String prezime);
	
	Student findByUser(User user);
	
	List<Ispit> prijaviIspit(PrijaviDTO prijavaDTO, String token);
	
	List<Ispit> getIspiti(String token);
	
	Uplata uplati(String token, UplataDTO uplataDTO);
	
	List<Uplata> getUplate(String token);
	
	List<Ispit> getMoguceIspite(String token);

	List<Ispit> odjaviIspit(PrijaviDTO prijavaDTO, String token);
	
	List<Ispit> getNepolozene(String token);
	
	List<Ispit> getPolozeni(String token);
}
