package com.project.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.dto.IspitDTO;
import com.project.dto.PredmetDTO;
import com.project.model.Ispit;
import com.project.model.Predavac;
import com.project.model.Predmet;
import com.project.model.User;
import com.project.repository.PredavacRepository;
import com.project.security.TokenUtils;

@Service
public class PredavacServiceImpl implements PredavacService {

	@Autowired
	private PredavacRepository repo;
	@Autowired
	private TokenUtils tokenUtils;
	@Autowired
	private UserService userService;
	@Autowired
	private IspitService ispitService;
	
	public Predavac findOne(Long id) {
		return repo.findById(id).get();
	}

	public List<Predavac> findAll() {
		return repo.findAll();
	}

	public Predavac save(Predavac predavac) {
		return repo.save(predavac);
	}

	public void remove(Long id) {
		repo.deleteById(id);
	}

	public Page<Predavac> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Predavac findByUser(User user) {
		return repo.findByUser(user);
	}
	

	@Override
	public List<PredmetDTO> getPredmeti(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Predavac p = repo.findByUser(user);
		return p.getPredaje().stream()
				.map(pr -> new PredmetDTO(pr))
				.collect(Collectors.toList());
	}

	@Override
	public List<IspitDTO> getZakazane(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Predavac p = repo.findByUser(user);
		List<Ispit> ispiti = ispitService.findAll();
		List<Ispit> ret = new ArrayList<>();
		for (Ispit i : ispiti) {
			if(p.getPredaje().contains(i.getPredmet())) {
				ret.add(i);
			}
		}
		return ret.stream().map(is -> new IspitDTO(is)).collect(Collectors.toList());
	}

	@Override
	public IspitDTO oceni(String token, Long id, Integer ocena) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Predavac p = repo.findByUser(user);
		Ispit i = ispitService.findOne(id);
		i.setOcena(ocena);
		ispitService.save(i);
		return new IspitDTO(i);
	}
}
