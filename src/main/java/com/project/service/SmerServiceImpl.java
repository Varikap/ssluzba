package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Smer;
import com.project.repository.SmerRepository;
import com.project.utils.Const;

@Service
public class SmerServiceImpl implements SmerService {

	@Autowired
	SmerRepository repo;

	public Smer findOne(Long id) {
		return repo.findById(id).get();
	}

	public List<Smer> findAll() {
		return repo.findAll();
	}

	public Smer save(Smer smer) {
		switch (smer.getNivo()) {
			case STRUKOVNE: {
				smer.setTrajanje(3);
				break;
			}
			case DOKTORSKE: {
				smer.setTrajanje(2);
				break;
			}
			case MASTER: {
				smer.setTrajanje(1);
				break;
			}
			case OSNOVNE: {
				smer.setTrajanje(4);
				break;
			}
			case ÜBERMEISTERSCHNITZELNLACHEN: {
				smer.setTrajanje(2);
				break;
			}
			default: {
				throw new IllegalArgumentException("Smer nivo not valid");
			}
		}
		return repo.save(smer);
	}

	public void remove(Long id) {
		repo.deleteById(id);
	}

	public Page<Smer> findAll(Pageable page) {
		return repo.findAll(page);
	}
	
}
