package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Dokument;

public interface DokumentService {
	Dokument findOne(Long id);

	List<Dokument> findAll();
	
	Page<Dokument> findAll(Pageable page);

	Dokument save(Dokument dokument);

	void remove(Long id);
}
