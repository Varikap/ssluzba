package com.project.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.dto.PrijaviDTO;
import com.project.dto.UplataDTO;
import com.project.model.Ispit;
import com.project.model.Predmet;
import com.project.model.Student;
import com.project.model.Uplata;
import com.project.model.User;
import com.project.repository.StudentRepository;
import com.project.security.TokenUtils;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository repo;
	@Autowired
	private IspitService ispitService;
	@Autowired
	private TokenUtils tokenUtils;
	@Autowired
	private UserService userService;
	@Autowired
	private UplataService uplataService;
	public Student findOne(Long id) {
		return repo.findById(id).get();
	}

	public List<Student> findAll() {
		return repo.findAll();
	}

	public Student save(Student student) {
		return repo.save(student);
	}

	public void remove(Long id) {
		repo.deleteById(id);
	}

	public Page<Student> findAll(Pageable page) {
		return repo.findAll(page);
	}

	public List<Student> findAllByIme(String ime) {
		return repo.findAllByName(ime);
	}

	public List<Student> findAllByPrezime(String prezime) {
		return repo.findAllBySurname(prezime);
	}

	@Override
	public Student findByUser(User user) {
		return repo.findByUser(user);
	}

	@Override
	public List<Ispit> prijaviIspit(PrijaviDTO prijavaDTO, String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Ispit> ispiti = new ArrayList<>();
		for (Long prijavaID : prijavaDTO.getIspitIDs()) {
			Ispit ispit = ispitService.findOne(prijavaID);
			student.addIspit(ispit);
			if (student.getStanjeNaRacunu() > (student.getStanjeNaRacunu() - ispit.getRok().getCena()))
				student.setStanjeNaRacunu(student.getStanjeNaRacunu() - ispit.getRok().getCena());
			ispiti.add(ispit);
			ispit.addStudent(student);
			ispitService.save(ispit);
		}
		repo.save(student);
		return ispiti;
	}

	@Override
	public List<Ispit> getIspiti(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Ispit> ispiti = ispitService.findAll();
		List<Ispit> ret = new ArrayList<>();
		for (Ispit ispit : ispiti) {
			for(Student s : ispit.getStudenti()) {
				if (student.getJmbg().equals(s.getJmbg())) {
					ret.add(ispit);
					break;
				}
			}
		}
		return ret;
	}

	@Override
	public Uplata uplati(String token, UplataDTO uplataDTO) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		student.setStanjeNaRacunu(student.getStanjeNaRacunu() + uplataDTO.getIznos());
		student = repo.save(student);
		Uplata u = new Uplata(uplataDTO.getSvrhaUplate(), uplataDTO.getIznos());
		u.setStudent(student);
		return uplataService.save(u);
	}

	@Override
	public List<Uplata> getUplate(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Uplata> uplate = uplataService.findAllByStudent(student);
		return uplate;
	}

	@Override
	public List<Ispit> getMoguceIspite(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Predmet> predmeti = student.getPohadja();
		List<Ispit> ispiti = ispitService.findAll();
		List<Ispit> returnVal = new ArrayList<>();
		for (Ispit i : ispiti) {
			if (predmeti.contains(i.getPredmet()) && !i.getStudenti().contains(student)) {
				returnVal.add(i);
			}
		}
		return returnVal;
	}

	@Override
	public List<Ispit> odjaviIspit(PrijaviDTO prijavaDTO, String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Predmet> studentPredmeti = student.getPohadja();
		List<Ispit> ispiti = new ArrayList<>();
		for (Long prijavaID : prijavaDTO.getIspitIDs()) {
			Ispit ispit = ispitService.findOne(prijavaID);
			student.setStanjeNaRacunu(student.getStanjeNaRacunu() + ispit.getRok().getCena());
			ispit.getStudenti().remove(student);
			student.getIspiti().remove(ispit);
			ispitService.save(ispit);
		}
		repo.save(student);
		return ispiti;
	}

	@Override
	public List<Ispit> getNepolozene(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Ispit> ispiti = ispitService.findAll(); 
		List<Ispit> retVal = new ArrayList<>();
		for(Ispit i : ispiti) {
			if(student.getIspiti().contains(i)) {
				if(i.getOcena() <= 5) {
					retVal.add(i);
				}
			}
		}
		return retVal;
	}

	@Override
	public List<Ispit> getPolozeni(String token) {
		User user = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Student student = repo.findByUser(user);
		List<Ispit> ispiti = ispitService.findAll();
		List<Ispit> retVal = new ArrayList<>();
		for(Ispit i : ispiti) {
			if(student.getIspiti().contains(i)) {
				if(i.getOcena() > 5) {
					retVal.add(i);
				}
			}
		}
		return retVal;
	}
}
