package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.project.model.Student;
import com.project.model.Uplata;
import com.project.repository.UplataRepository;

@Service
public class UplataServiceImpl implements UplataService {
	
	@Autowired
	UplataRepository repo;
	
	@Override
	public Uplata findOne(Long id) {
		return repo.findById(id).get();
	}

	@Override
	public List<Uplata> findAll() {
		return repo.findAll();
	}

	@Override
	public Page<Uplata> findAll(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Uplata save(Uplata uplata) {
		return repo.save(uplata);
	}

	@Override
	public void remove(Long id) {
		repo.deleteById(id);
	}

	@Override
	public List<Uplata> findAllByStudent(Student student) {
		return repo.findAllByStudent(student);
	}

}
