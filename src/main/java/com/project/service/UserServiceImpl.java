package com.project.service;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.project.model.Authority;
import com.project.model.User;
import com.project.model.UserAuthority;
import com.project.model.enums.Role;
import com.project.repository.AuthorityRepository;
import com.project.repository.UserAuthorityRepository;
import com.project.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	UserAuthorityRepository userAuthRepo;
	
	@Autowired
	AuthorityRepository authRepo;
	
	@Override
	public User save(String username, String password, Role role) {
		User u = new User();
		u.setUsername(username);
		u.setPassword(passwordEncoder.encode(password));
		Authority auth = authRepo.getOneByRole(role);
		UserAuthority uauth = new UserAuthority();
		uauth.setUser(u);
		uauth.setAuthority(auth);
		HashSet<UserAuthority> set = new HashSet<UserAuthority>();
		set.add(uauth);
		u.setUserAuthorities(set);
		userAuthRepo.save(uauth);
		userRepo.save(u);
		return u;
	}

	@Override
	public User findByUsername(String username) {
		return userRepo.findByUsername(username);
	}

	@Override
	public User save(User u) {
		return userRepo.save(u);
	}
}
