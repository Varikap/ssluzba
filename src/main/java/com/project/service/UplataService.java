package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Student;
import com.project.model.Uplata;

public interface UplataService {
	Uplata findOne(Long id);

	List<Uplata> findAll();

	Page<Uplata> findAll(Pageable page);

	Uplata save(Uplata uplata);

	void remove(Long id);
	
	List<Uplata> findAllByStudent(Student student);
}
