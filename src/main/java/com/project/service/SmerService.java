package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Smer;

public interface SmerService {
	Smer findOne(Long id);

	List<Smer> findAll();
	
	Page<Smer> findAll(Pageable page);

	Smer save(Smer smer);

	void remove(Long id);
}
