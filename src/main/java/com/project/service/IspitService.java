package com.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.project.model.Ispit;

public interface IspitService {
	Ispit findOne(Long id);

	List<Ispit> findAll();
	
	Page<Ispit> findAll(Pageable page);

	Ispit save(Ispit ispit);

	void remove(Long id);
}
