package com.project.utils;

public final class Const {
	public static final String NASTAVNIK = "Nastavnik";
	public static final String ASISTENT = "Asistent";
	public static final String DEMONSTRATOR = "Demonstrator";
	public static final String DOKTORSKE = "Doktorske";
	public static final String MASTER = "Master";
	public static final String OSNOVNE = "Osnovne";
	public static final String ÜBERMEISTERSCHNITZELNLACHEN = "Übermeisterschnitzelnlachen";
	public static final String STRUKOVNE = "STRUKOVNE";
}

