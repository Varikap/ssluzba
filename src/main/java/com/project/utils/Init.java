package com.project.utils;


import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.project.model.Admin;
import com.project.model.Dokument;
import com.project.model.Ispit;
import com.project.model.Predavac;
import com.project.model.Predmet;
import com.project.model.Rok;
import com.project.model.Smer;
import com.project.model.Student;
import com.project.model.Uplata;
import com.project.model.User;
import com.project.model.enums.NivoStudija;
import com.project.model.enums.Zvanje;
import com.project.service.AdminService;
import com.project.service.DokumentService;
import com.project.service.IspitService;
import com.project.service.PredavacService;
import com.project.service.PredmetService;
import com.project.service.RokService;
import com.project.service.SmerService;
import com.project.service.StudentService;
import com.project.service.UplataService;
import com.project.service.UserService;

@Component
public class Init {

	@Autowired
	private DokumentService docS;
	@Autowired
	private PredavacService predavacS;
	@Autowired
	private PredmetService predmetS;
	@Autowired
	private SmerService smerS;
	@Autowired
	private StudentService studentS;
	@Autowired 
	private UplataService uplataS;
	@Autowired 
	private IspitService ispitS;
	@Autowired
	private UserService userS;
	@Autowired
	private AdminService adminS;
	@Autowired
	private RokService rokService;
	
	@PostConstruct
	private void init() {
		User studentU = userS.findByUsername("user");
		User adminU = userS.findByUsername("admin");
		User predavacU = userS.findByUsername("predavac");
		////////////////////////
		Smer s = smerS.save(new Smer("E1",NivoStudija.OSNOVNE,8,240));
		Smer s1 = smerS.save(new Smer("E2",NivoStudija.OSNOVNE,8,240));
		Smer s2 = smerS.save(new Smer("E3",NivoStudija.ÜBERMEISTERSCHNITZELNLACHEN,8,240));
		////////////////////////
		Predmet p = predmetS.save(new Predmet("Matomatika",s,"asdads",4,1));
		Predmet p1 = predmetS.save(new Predmet("Matomatika",s1,"asdads",4,2));
		Predmet p2 = predmetS.save(new Predmet("Turbotronika",s,"asdads",4,3));
		Predmet p3 = predmetS.save(new Predmet("Nitrotronika",s1,"asdads",4,1));
		Predmet p4 = predmetS.save(new Predmet("Plebotronika",s1,"asdads",4,2));
		Predmet p5 = predmetS.save(new Predmet("Memetronika",s2,"asdads",4,3));
		Predmet p6 = predmetS.save(new Predmet("Pepetronika",s2,"asdads",4,1));
		Predmet p7 = predmetS.save(new Predmet("Nenamvisenista",s2,"asdads",4,2));
		Predmet p8 = predmetS.save(new Predmet("Predmemdekwmtke",s,"asdads",4,4));
		////////////////////////
		Predavac pr = predavacS.save(new Predavac("ime", "surname", "jmbg",Zvanje.NASTAVNIK, "predavac1@gmail.com"));
		Predavac pr1 = predavacS.save(new Predavac("ime2", "surname2", "jmbg2",Zvanje.ASISTENT, "predavac2@gmail.com"));
		Predavac pr2 = predavacS.save(new Predavac("ime3", "surname3", "jmbg3",Zvanje.NASTAVNIK,predavacU ,"predavac2@gmail.com"));
		pr.addPredmet(p8);
		pr.addPredmet(p7);
		pr.addPredmet(p6);
		pr.addPredmet(p5);
		pr1.addPredmet(p5);
		pr1.addPredmet(p4);
		pr1.addPredmet(p3);
		pr1.addPredmet(p2);
		pr2.addPredmet(p2);
		pr2.addPredmet(p3);
		pr2.addPredmet(p1);
		pr2.addPredmet(p);
		pr = predavacS.save(pr);
		pr1 = predavacS.save(pr1);
		pr2 = predavacS.save(pr2);
		////////////////////////
		Student st;
		for(int i=0 ;i<20;i++) {
			if (i==0) {
				st = new Student("name"+i, "surname"+i, "6634w24"+i, "EE/ee/"+i, i%2==0 ? s1 : s2, studentU);
				st.setStanjeNaRacunu(0.0);
				st.setGodina(1);
				st = studentS.save(st);
				st.addPredmet(p8);
				st.addPredmet(p7);
				st.addPredmet(p6);
				st.addPredmet(p5);
				st.addPredmet(p4);
				st.addPredmet(p3);
				st.addPredmet(p6);
				st.addPredmet(p);
				st = studentS.save(st);
				Rok	rok = rokService.save(new Rok("rok", new Date(2019, 1,1), new Date(2019, 12,29), 20));
				Ispit ispit = new Ispit(new Date(), 0 , st, p);
				ispit.setRok(rok);
				ispit = ispitS.save(ispit);
				continue;
			}
			st = new Student("name"+i, "surname"+i, "6634w24"+i, "EE/ee/"+i, i%2==0 ? s1 : s2);
			st.setStanjeNaRacunu(0.0);
			st.setGodina(2);
			st = studentS.save(st);
			if(i%2==0) {
				st.addPredmet(p8);
				st.addPredmet(p7);
				st.addPredmet(p6);
				st.addPredmet(p5);
			}else {
				st.addPredmet(p4);
				st.addPredmet(p3);
				st.addPredmet(p6);
				st.addPredmet(p);
			}
			for(int x = 0 ; x<6;++x) {
				docS.save(new Dokument("Svedocanstvo"+x, st));
				uplataS.save(new Uplata(x%2==0?"Upis":"Ispit", 3301, st));
			}
			Rok	rok = rokService.save(new Rok("rok"+i, new Date(2019, 5+i,10+i), new Date(2019, 1+i,1+i), 20+i));
			Ispit ispit = new Ispit(new Date(), i%10+1 , st, i%2==0 ? p1 : p2);
			ispit.setRok(rok);
			ispit = ispitS.save(ispit);
			studentS.save(st);
		}
		////////////////////////
		Admin a;
		for (int i=0 ;i<6;i++) {
			if (i==0) {
				a = adminS.save(new Admin("admin"+i, "admin"+i, "2131245s"+i, "admin"+i+"@admin.com", adminU));
				continue;
			}
			a = adminS.save(new Admin("admin"+i, "admin"+i, "2131245s"+i, "admin"+i+"@admin.com"));
		}
		
	}
}
 