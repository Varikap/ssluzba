package com.project.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.AdminConverter;
import com.project.converters.CreateAdminConverter;
import com.project.converters.CreatePredavacConverter;
import com.project.converters.CreateStudentConverter;
import com.project.dto.AdminDTO;
import com.project.dto.CreateAdminDTO;
import com.project.dto.CreatePredavacDTO;
import com.project.dto.CreateStudentDTO;
import com.project.dto.LoginDTO;
import com.project.dto.NovaSifra;
import com.project.dto.PredavacDTO;
import com.project.dto.StudentDTO;
import com.project.dto.TokenDTO;
import com.project.model.Admin;
import com.project.model.Authority;
import com.project.model.Predavac;
import com.project.model.Student;
import com.project.model.User;
import com.project.model.UserAuthority;
import com.project.model.enums.Role;
import com.project.security.TokenUtils;
import com.project.service.AdminService;
import com.project.service.PredavacService;
import com.project.service.StudentService;
import com.project.service.UserService;

@RestController
@RequestMapping(value = "api")
public class UserController {
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private TokenUtils tokenUtils;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private CreateStudentConverter studentConverter;
	
	@Autowired
	private PredavacService predavacService;
	
	@Autowired
	private CreatePredavacConverter predavacConverter;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private CreateAdminConverter adminConverter;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	@PostMapping(value="/login", consumes = {"application/json"})
	public ResponseEntity<TokenDTO> login(@RequestBody @Valid LoginDTO loginDTO) {
        try {
			UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(), loginDTO.getPassword());
            Authentication authentication = authenticationManager.authenticate(upToken);
            UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
            var authority = (GrantedAuthority) details.getAuthorities().toArray()[0];
            if(!authority.getAuthority().equals(loginDTO.getRole().name()))
            	return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            TokenDTO tokenDTO = new TokenDTO();
            String token = tokenUtils.generateToken(details);
            tokenDTO.setToken(token);
            tokenDTO.setExpiresIn(tokenUtils.getExpirationTime());
            return new ResponseEntity<TokenDTO>(tokenDTO, HttpStatus.OK);
        } catch (Exception ex) {
        	System.out.println(ex.getStackTrace());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
	}
	
	@PostMapping(value="/create/student", consumes = {"application/json"})
	public ResponseEntity<Void> createStudent(@RequestBody @Valid CreateStudentDTO studentDTO) {
		// TODO username exists
		try {
			Student student = studentConverter.convert(studentDTO);
			User u = userService.save(studentDTO.getIndexBr(), studentDTO.getJmbg(), Role.STUDENT);
			student.setUser(u);
			student = studentService.save(student);
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping(value="/create/predavac")
	public ResponseEntity<Void> createPredavac (@RequestBody @Valid CreatePredavacDTO predavacDTO) {
		try {
			Predavac p = predavacService.save(predavacConverter.convert(predavacDTO));
			User u = userService.save(predavacDTO.getUsername(), predavacDTO.getPassword(), Role.PREDAVAC);
			p.setUser(u);
			p = predavacService.save(p);
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping(value="/create/admin", consumes = "application/json")
	public ResponseEntity<Void> createAdmin (@RequestBody @Valid CreateAdminDTO adminDTO) {
		try {
			Admin admin = adminConverter.convert(adminDTO);
			User u = userService.save(adminDTO.getUsername(), adminDTO.getPassword(), Role.ADMIN);
			admin.setUser(u);
			admin = adminService.save(admin);
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/ulogovan")
	ResponseEntity<Object> getLoggedIn(@RequestHeader("X-Auth-Token") String token){
		User u = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
		Role role = null;
		for (UserAuthority ua : u.getUserAuthorities()) {
			role = ua.getAuthority().getRole();
		}
		switch (role) {
		case STUDENT:
			Student student = studentService.findByUser(u);
			StudentDTO dto = new StudentDTO(student);
			dto.setRole(role.name());
			return new ResponseEntity<Object>(dto, HttpStatus.OK);

		case PREDAVAC:
			Predavac predavac = predavacService.findByUser(u);
			PredavacDTO dtoP = new PredavacDTO(predavac);
			dtoP.setRole(role.name());
			return new ResponseEntity<Object>(dtoP, HttpStatus.OK);
			
		case ADMIN:
			Admin admin = adminService.findByUser(u);
			AdminDTO dtoA = new AdminDTO(admin);
			dtoA.setRole(role.name());
			return new ResponseEntity<Object>(dtoA,HttpStatus.OK);
		default:
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/nova")
	ResponseEntity<Void> novaSifra(@RequestHeader("X-Auth-Token") String token,
			@RequestBody NovaSifra novaSifra) {
		try {
			User u = userService.findByUsername(tokenUtils.getUsernameFromToken(token));
			u.setPassword(passwordEncoder.encode(novaSifra.getNovaSifra()));
			userService.save(u);
			return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
