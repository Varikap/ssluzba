package com.project.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.RokConverter;
import com.project.dto.RokDTO;
import com.project.model.Rok;
import com.project.service.RokService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/rok")
@Log4j2
public class RokController {
	@Autowired
	private RokService rokService;
	
	@Autowired
	private RokConverter rokConverter;
	
	@GetMapping(value = "/{id}")
	ResponseEntity<RokDTO> getOne(@PathVariable Long id) {
		Rok r;
		try {
			r = rokService.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<RokDTO>(new RokDTO(r), HttpStatus.OK);
	}
	
	@GetMapping(value = "/all")
	ResponseEntity<List<RokDTO>> getAll() {
		List<RokDTO> dto = rokService.findAll()
				.stream()
				.map(r -> new RokDTO(r))
				.collect(Collectors.toList());
		return new ResponseEntity<List<RokDTO>>(dto, HttpStatus.OK);
	}
	
	@PostMapping(consumes = "application/json")
	ResponseEntity<RokDTO> create(@RequestBody RokDTO rokDTO) {
		try {
			rokDTO.setId(null);
			Rok rok = rokService.save(rokConverter.convert(rokDTO));
			return new ResponseEntity<RokDTO>(new RokDTO(rok), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{id}" ,consumes = "application/json")
	ResponseEntity<RokDTO> update(@RequestBody RokDTO rokDTO, @PathVariable Long id) {
		if(id!=rokDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Rok rok = rokService.save(rokConverter.convert(rokDTO));
			return new ResponseEntity<RokDTO>(new RokDTO(rok), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	ResponseEntity<RokDTO> delete(@PathVariable Long id) {
		try {
			rokService.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
