package com.project.controller;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.AdminConverter;
import com.project.dto.AdminDTO;
import com.project.model.Admin;
import com.project.service.AdminService;

@RestController
@RequestMapping(value="api/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private AdminConverter adminConverter;

	@GetMapping(value = "/{id}")
	ResponseEntity<AdminDTO> getOne(@PathVariable Long id) {
		Admin a;
		try {
			a = adminService.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new AdminDTO(a), HttpStatus.OK);
	}
	
	@GetMapping(value="/all")
	ResponseEntity<List<AdminDTO>> getAll() {
		// TODO NOT FOUND
		List<Admin> a = adminService.findAll();
		List<AdminDTO> dto = null;
		dto = a.stream().map(ad -> new AdminDTO(ad)).collect(Collectors.toList());
		return new ResponseEntity<List<AdminDTO>>(dto, HttpStatus.OK);
	}
	
	@DeleteMapping(value="{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {
		// TODO NOT FOUND
		adminService.remove(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	ResponseEntity<AdminDTO> create (@RequestBody AdminDTO adminDTO) {
		Admin admin = adminService.save(adminConverter.convert(adminDTO));
		return new ResponseEntity<AdminDTO>(new AdminDTO(admin), HttpStatus.CREATED);
	}
	
	@PutMapping(consumes = "application/json", value = "/{id}")
	ResponseEntity<AdminDTO> edit(@RequestBody AdminDTO adminDTO, @PathVariable Long id) {
		if(id!=adminDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Admin a = adminService.save(adminConverter.convert(adminDTO));
			return new ResponseEntity<AdminDTO>(new AdminDTO(a), HttpStatus.OK);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
