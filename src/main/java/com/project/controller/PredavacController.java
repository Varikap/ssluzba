package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.PredavacConverter;
import com.project.dto.IspitDTO;
import com.project.dto.PredavacDTO;
import com.project.dto.PredmetDTO;
import com.project.model.Predavac;
import com.project.model.enums.Zvanje;
import com.project.service.PredavacService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/predavaci")
@Log4j2
public class PredavacController {
	@Autowired
	private PredavacService predavacS;
	@Autowired
	private PredavacConverter predavacC;

	@GetMapping(value = "/{id}")
	ResponseEntity<PredavacDTO> getOne(@PathVariable Long id) {
		Predavac p;
		try {
			p = predavacS.findOne(id);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new PredavacDTO(p), HttpStatus.OK);
	}
	
	@GetMapping(value = "/all")
	ResponseEntity<List<PredavacDTO>> getAll() {
		List<Predavac> p = null;
		List<PredavacDTO> dto = null;
		try {
			p = predavacS.findAll();
			dto = new ArrayList<PredavacDTO>();
			for (Predavac pr : p)
				dto.add(new PredavacDTO(pr));
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			predavacS.remove(id);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(consumes = "application/json")
	ResponseEntity<PredavacDTO> create(@RequestBody PredavacDTO p) {
		try {
			p.setId(null);
			Predavac pr = predavacS.save(predavacC.convert(p));
			return new ResponseEntity<>(new PredavacDTO(pr), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping(value = "/{id}", consumes = "application/json")
	ResponseEntity<PredavacDTO> edit(@RequestBody PredavacDTO p, @PathVariable Long id) {
		if (id != p.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Predavac pr = predavacS.save(predavacC.convert(p));
			return new ResponseEntity<>(new PredavacDTO(pr), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	

	@GetMapping(value="/zvanje")
	ResponseEntity<List<String>> getZvanje(){
		List<String> dto = new ArrayList<String>();
		for (Zvanje z : Zvanje.values())
			dto.add(z.toString());
		return new ResponseEntity<List<String>>(dto, HttpStatus.OK);
	}
	
	@GetMapping(value="/predmeti")
	ResponseEntity<List<PredmetDTO>> getPredmeti(@RequestHeader("X-Auth-Token") String token){
		try {
			return new ResponseEntity<List<PredmetDTO>>(predavacS.getPredmeti(token), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value="/zakazani")
	ResponseEntity<List<IspitDTO>> getZakazane(@RequestHeader("X-Auth-Token") String token){
		try {
			return new ResponseEntity<List<IspitDTO>>(predavacS.getZakazane(token), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/oceni/{id}")
	ResponseEntity<IspitDTO> oceni(@RequestHeader("X-Auth-Token") String token,
			@PathVariable Long id,
			@RequestParam Integer ocena) {
		try {
			return new ResponseEntity<IspitDTO>(predavacS.oceni(token, id, ocena), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
