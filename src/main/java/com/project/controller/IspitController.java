package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.IspitConverter;
import com.project.dto.IspitDTO;
import com.project.model.Ispit;
import com.project.service.IspitService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/ispiti")
@Log4j2
public class IspitController {
	@Autowired
	private IspitService ispitService;
	@Autowired
	private IspitConverter ispitConverter;
	
	
	
	@GetMapping(value = "/all")
	ResponseEntity<List<IspitDTO>> getAll() {
		List<Ispit> i = null;
		List<IspitDTO> dto = null;
		try {
			i = ispitService.findAll();
			dto = new ArrayList<IspitDTO>();
			for (Ispit isp : i)
				dto.add(new IspitDTO(isp));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/{id}")
	ResponseEntity<IspitDTO> getOne(@PathVariable Long id) {
		Ispit i;
		try {
			i = ispitService.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new IspitDTO(i), HttpStatus.OK);
	}
	
	
	@PostMapping(consumes = "application/json")
	ResponseEntity<IspitDTO> create(@RequestBody IspitDTO i) {
		try {
			i.setId(null);
			Ispit isp = ispitService.save(ispitConverter.convert(i));
			return new ResponseEntity<>(new IspitDTO(isp), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@PutMapping(value = "/{id}", consumes = "application/json")
	ResponseEntity<IspitDTO> edit(@RequestBody IspitDTO i, @PathVariable Long id) {
		if (id != i.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Ispit isp = ispitService.save(ispitConverter.convert(i));
			return new ResponseEntity<>(new IspitDTO(isp), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@DeleteMapping(value = "/{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			ispitService.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
