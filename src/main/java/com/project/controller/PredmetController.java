package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.PredmetConverter;
import com.project.dto.PredmetDTO;
import com.project.model.Predmet;
import com.project.service.PredmetService;
import com.project.service.SmerService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/predmeti")
@Log4j2
public class PredmetController {
	@Autowired
	private PredmetService predmetService;
	@Autowired
	private PredmetConverter predmetConverter;
	@Autowired
	private SmerService smerService;
	
	@GetMapping
	public ResponseEntity<List<PredmetDTO>> getAllPage(Pageable page) {
		Page<Predmet> predmeti = predmetService.findAll(page);
		List<PredmetDTO> predmetiDTO = predmeti
				.stream()
				.map(p -> new PredmetDTO(p))
				.collect(Collectors.toList());
		return new ResponseEntity<List<PredmetDTO>>(predmetiDTO, HttpStatus.OK);
	}
	
	@GetMapping(value = "/all")
	public ResponseEntity<List<PredmetDTO>> getAll() {
		List<Predmet> predmeti = new ArrayList<Predmet>();
		List<PredmetDTO> predmetiDTO = new ArrayList<PredmetDTO>();
		try {
			predmeti = predmetService.findAll();
			predmetiDTO = predmeti
					.stream()
					.map(p -> new PredmetDTO(p))
					.collect(Collectors.toList());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<PredmetDTO>>(predmetiDTO, HttpStatus.OK);
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<PredmetDTO> findOne(@PathVariable Long id) {
		Predmet p;
		try {
			p = predmetService.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<PredmetDTO>(new PredmetDTO(p), HttpStatus.OK);
	}
	
	@PostMapping(consumes="application/json")
	public ResponseEntity<PredmetDTO> savePredmet(@RequestBody PredmetDTO predmetDTO){
		try {
			predmetDTO.setId(null);
			Predmet predmet = predmetConverter.convert(predmetDTO);
			predmet.setSmer(smerService.findOne(predmetDTO.getSmerId()));
			predmet = predmetService.save(predmet);
			return new ResponseEntity<PredmetDTO>(new PredmetDTO(predmet), HttpStatus.CREATED); 
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(consumes="application/json", value = "/{id}")
	public ResponseEntity<PredmetDTO> updatePredmet(@RequestBody PredmetDTO predmetDTO, @PathVariable Long id) {
		if(id != predmetDTO.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Predmet predmet = predmetConverter.convert(predmetDTO);
			predmet.setSmer(smerService.findOne(predmetDTO.getSmerId()));
			predmet = predmetService.save(predmet);
			return new ResponseEntity<PredmetDTO>(new PredmetDTO(predmet), HttpStatus.CREATED); 
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Void> removePredmet(@PathVariable Long id) {
		try {
			predmetService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
