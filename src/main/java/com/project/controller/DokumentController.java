package com.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.DokumentConverter;
import com.project.dto.DokumentDTO;
import com.project.model.Dokument;
import com.project.service.DokumentService;

@RestController
@RequestMapping(value = "/api/document")
public class DokumentController {
	@Autowired
	private DokumentService dokumentS;
	@Autowired
	private DokumentConverter dokumentC;

	@GetMapping(value = "/{id}")
	ResponseEntity<DokumentDTO> getOne(@PathVariable Long id) {
		Dokument d;
		try {
			d = dokumentS.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new DokumentDTO(d), HttpStatus.OK);
	}

	@GetMapping(value = "/all")
	ResponseEntity<List<DokumentDTO>> getAll() {
		List<Dokument> d = null;
		List<DokumentDTO> dto = null;
		try {
			d = dokumentS.findAll();
			dto = new ArrayList<DokumentDTO>();
			for (Dokument doc : d)
				dto.add(new DokumentDTO(doc));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			dokumentS.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(consumes = "application/json")
	ResponseEntity<DokumentDTO> create(@RequestBody DokumentDTO d) {
		try {
			d.setId(null);
			Dokument doc = dokumentS.save(dokumentC.convert(d));
			return new ResponseEntity<>(new DokumentDTO(doc), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping(value = "/{id}", consumes = "application/json")
	ResponseEntity<DokumentDTO> edit(@RequestBody DokumentDTO d, @PathVariable Long id) {
		if (id != d.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Dokument doc = dokumentS.save(dokumentC.convert(d));
			return new ResponseEntity<>(new DokumentDTO(doc), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
