package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.CreateStudentConverter;
import com.project.converters.StudentConverter;
import com.project.dto.CreateStudentDTO;
import com.project.dto.IspitDTO;
import com.project.dto.PrijaviDTO;
import com.project.dto.StudentDTO;
import com.project.dto.UplataDTO;
import com.project.model.Ispit;
import com.project.model.Student;
import com.project.model.Uplata;
import com.project.model.User;
import com.project.security.TokenUtils;
import com.project.service.StudentService;
import com.project.service.UserService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/studenti")
@Log4j2
public class StudentController {
	@Autowired
	private StudentService studentS;
	
	@Autowired
	private StudentConverter studentC;
	
	@Autowired
	private CreateStudentConverter createStudentConverter;
	
	
	@GetMapping(value = "/{id}")
	ResponseEntity<StudentDTO> getOne(@PathVariable Long id) {
		Student s;
		try {			
			s = studentS.findOne(id);
		} catch (Exception e) {		
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new StudentDTO(s), HttpStatus.OK);
	}
	@GetMapping(value = "/all")
	ResponseEntity<List<StudentDTO>> getAll() {
		List<Student> s = null;
		List<StudentDTO> dto = null;
		try {
			s = studentS.findAll();
			dto = new ArrayList<StudentDTO>();
			for(Student st : s)
				dto.add(new StudentDTO(st));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	@DeleteMapping(value = "/{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {
		try {			
			studentS.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(consumes = "application/json", value = "/{id}")
	ResponseEntity<StudentDTO> edit(@RequestBody CreateStudentDTO s,
			@PathVariable Long id) {
		if(id!=s.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			User user = studentS.findOne(id).getUser();
			Student st = createStudentConverter.convert(s);
			st.setUser(user);
			st = studentS.save(st);
			return new ResponseEntity<>(new StudentDTO(st),HttpStatus.ACCEPTED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/uplata", consumes = "application/json")
	ResponseEntity<UplataDTO> uplata(@RequestHeader("X-Auth-Token") String token,
			@RequestBody UplataDTO uplataDTO) {
		try {
			Uplata u = studentS.uplati(token, uplataDTO);
			return new ResponseEntity<UplataDTO>(new UplataDTO(u), HttpStatus.ACCEPTED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/prijavi", consumes = "application/json")
	ResponseEntity<List<IspitDTO>> prijavi(@RequestHeader("X-Auth-Token") String token,
			@RequestBody PrijaviDTO prijavaDTO) {
		try {
			List<Ispit> ispiti = studentS.prijaviIspit(prijavaDTO, token);
			List<IspitDTO> dto = ispiti.stream()
					.map(i -> new IspitDTO(i)).collect(Collectors.toList());
			return new ResponseEntity<List<IspitDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/odjavi", consumes = "application/json")
	ResponseEntity<List<IspitDTO>> odjavi(@RequestHeader("X-Auth-Token") String token,
			@RequestBody PrijaviDTO prijavaDTO) {
		try {
			List<Ispit> ispiti = studentS.odjaviIspit(prijavaDTO, token);
			List<IspitDTO> dto = ispiti.stream()
					.map(i -> new IspitDTO(i)).collect(Collectors.toList());
			return new ResponseEntity<List<IspitDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/prijavljeni")
	ResponseEntity<List<IspitDTO>> getPrijavljenje(@RequestHeader("X-Auth-Token") String token) {
		try {
			List<Ispit> ispiti = studentS.getIspiti(token);
			List<IspitDTO> dto = ispiti.stream()
					.map(i -> new IspitDTO(i)).collect(Collectors.toList());
			return new ResponseEntity<List<IspitDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/nepolozeni")
	ResponseEntity<List<IspitDTO>> getNepolozeni(@RequestHeader("X-Auth-Token") String token) {
		try {
			List<Ispit> ispiti = studentS.getNepolozene(token);
			List<IspitDTO> dto = ispiti.stream()
					.map(i -> new IspitDTO(i)).collect(Collectors.toList());
			return new ResponseEntity<List<IspitDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/polozeni")
	ResponseEntity<List<IspitDTO>> getPolozeni(@RequestHeader("X-Auth-Token") String token) {
		try {
			List<Ispit> ispiti = studentS.getPolozeni(token);
			List<IspitDTO> dto = ispiti.stream()
					.map(i -> new IspitDTO(i)).collect(Collectors.toList());
			return new ResponseEntity<List<IspitDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/moguci")
	ResponseEntity<List<IspitDTO>> getIspite(@RequestHeader("X-Auth-Token") String token) {
		try {
			List<Ispit> ispiti = studentS.getMoguceIspite(token);
			List<IspitDTO> dto = ispiti.stream()
					.map(i -> new IspitDTO(i)).collect(Collectors.toList());
			return new ResponseEntity<List<IspitDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/istorijaUplata")
	ResponseEntity<List<UplataDTO>> getIstorijaUplata(@RequestHeader("X-Auth-Token") String token) {
		try {
			List<Uplata> uplate = studentS.getUplate(token);
			List<UplataDTO> dto = uplate.stream()
					.map(u -> new UplataDTO(u))
					.collect(Collectors.toList());
			return new ResponseEntity<List<UplataDTO>>(dto, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
