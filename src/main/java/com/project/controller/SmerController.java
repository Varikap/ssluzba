package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.converters.SmerConverter;
import com.project.dto.SmerDTO;
import com.project.model.Smer;
import com.project.model.enums.NivoStudija;
import com.project.service.SmerService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value = "api/smerovi")
@Log4j2
public class SmerController {
	@Autowired
	private SmerService smerService;
	@Autowired
	private SmerConverter smerConverter;
	

	@GetMapping(value = "/all")
	ResponseEntity<List<SmerDTO>> getAll() {
		List<Smer> s = null;
		List<SmerDTO> dto = null;
		try {
			s = smerService.findAll();
			dto = new ArrayList<SmerDTO>();
			for (Smer sm : s)
				dto.add(new SmerDTO(sm));
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/{id}")
	ResponseEntity<SmerDTO> getOne(@PathVariable Long id) {
		Smer s;
		try {
			s = smerService.findOne(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(new SmerDTO(s), HttpStatus.OK);
	}
	
	
	@PostMapping(consumes = "application/json")
	ResponseEntity<SmerDTO> create(@RequestBody SmerDTO s) {
		try {
			s.setId(null);
			Smer sm = smerService.save(smerConverter.convert(s));
			return new ResponseEntity<>(new SmerDTO(sm), HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value = "/{id}", consumes = "application/json")
	ResponseEntity<SmerDTO> edit(@RequestBody SmerDTO s, @PathVariable Long id) {
		if (id != s.getId())
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		try {
			Smer sm = smerService.save(smerConverter.convert(s));
			return new ResponseEntity<>(new SmerDTO(sm), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	ResponseEntity<Void> delete(@PathVariable Long id) {
		try {
			smerService.remove(id);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/nivoi")
	ResponseEntity<List<String>> getNivoe() {
		List<String> dto = new ArrayList<String>();
		for (NivoStudija n : NivoStudija.values())
			dto.add(n.toString());
		return new ResponseEntity<List<String>>(dto, HttpStatus.OK);
	}
}
