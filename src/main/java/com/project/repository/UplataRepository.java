package com.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Student;
import com.project.model.Uplata;

@Repository
public interface UplataRepository extends JpaRepository<Uplata, Long> {
	List<Uplata> findAllByStudent(Student student);
}
