package com.project.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Dokument;

@Repository
public interface DokumentRepository extends JpaRepository<Dokument, Long>{

}
