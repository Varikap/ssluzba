package com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Authority;
import com.project.model.enums.Role;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long>{
	Authority getOneByRole(Role role);
}
