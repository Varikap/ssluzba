package com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Predmet;

@Repository
public interface PredmetRepository extends JpaRepository<Predmet, Long> {

}
