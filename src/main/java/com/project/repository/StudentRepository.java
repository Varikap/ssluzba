package com.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Student;
import com.project.model.User;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
	List<Student> findAllByName(String ime);
	List<Student> findAllBySurname(String prezime);
	Student findByUser(User user);
}
