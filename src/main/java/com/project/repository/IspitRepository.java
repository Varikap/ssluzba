package com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.Ispit;

@Repository
public interface IspitRepository extends JpaRepository<Ispit, Long>{

}
